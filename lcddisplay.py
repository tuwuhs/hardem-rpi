#
# HardEm
#
# (c) 2015 Labtek Indie
# All rights reserved.

import time

import Adafruit_Nokia_LCD as LCD
import Adafruit_GPIO.SPI as SPI
import RPi.GPIO as GPIO

from PIL import Image

class LcdDisplay(object):
	def __init__(self):
		# Raspberry Pi hardware SPI config:
		DC = 23
		RST = 24
		SPI_PORT = 0
		SPI_DEVICE = 0
		self._BACKLIGHT_PIN = 22

		# Hardware SPI usage:
		self._disp = LCD.PCD8544(DC, RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=4000000))

		# Initialize library.
		self._disp.begin(contrast=60)
		
		# Initialize backlight GPIO
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self._BACKLIGHT_PIN, GPIO.OUT)
	
	def clear(self):
		self._disp.clear()
		self._disp.display()		
	
	def load_and_display_image(self, image_filename):
		self._disp.clear()
		self._disp.display()
		
		# Load an image, resize it, and convert to 1 bit color.
		image = Image.open(image_filename)
		ratio = max(1.0*LCD.LCDWIDTH/image.size[0], 1.0*LCD.LCDHEIGHT/image.size[1])
		image = image.resize((int(image.size[0]*ratio), int(image.size[1]*ratio)), Image.ANTIALIAS).convert('1')
		self.display_image(image)		   
	
	def display_image(self, image):
		self._disp.image(image)
		self._disp.display()
	
	def set_backlight(self, on):
		GPIO.output(self._BACKLIGHT_PIN, not on)
